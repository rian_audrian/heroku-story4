from django import forms
from .models import Event

class DateInput(forms.DateInput):
	input_type = 'date'

class TimeInput(forms.TimeInput):
	input_type = 'time'

class EventForm(forms.ModelForm):
	class Meta:
		model = Event
		fields = ['name', 'location', 'date', 'time', 'category']
		